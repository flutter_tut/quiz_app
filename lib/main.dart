import 'package:flutter/material.dart';
import './quiz.dart';
import './result.dart';

void main() => runApp(MyFirstApp());

class MyFirstApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyFirstAppState();
  }
}

class _MyFirstAppState extends State<MyFirstApp> {
  var _questionIndex = 0;
  var _totalScore = 0;
  void _showName(int score) {
    _totalScore += score;
    setState(() {
      _questionIndex = _questionIndex + 1;
      print('State Changed');
    });
    print('Name Shown');
  }

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
      print('Reset');
    });
  }

  @override
  Widget build(BuildContext context) {
    final _questions = const [
      {
        'qText': '1. what is your name?',
        'answers': [
          {'text': 'Rihaan', 'score': 1},
          {'text': 'Arshaan', 'score': 2},
        ],
      },
      {
        'qText': '2. what is your name?',
        'answers': [
          {'text': 'Azma', 'score': 1},
          {'text': 'Kishore', 'score': 2},
        ],
      },
    ];

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Quiz App'),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(
                questions: _questions,
                answerHandler: _showName,
                questionIndex: _questionIndex,
              )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
